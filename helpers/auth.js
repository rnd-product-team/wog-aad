const msal = require('@azure/msal-node');
const client = require('./connectDb');

const {
  AUTHENTICATION,
} = require('../constants');

const config = {
  auth: {
    clientId: AUTHENTICATION.clientId,
    authority: `https://login.microsoftonline.com/${AUTHENTICATION.tenantId}/`,
    knownAuthorities: [`https://login.microsoftonline.com/${AUTHENTICATION.tenantId}/`],
    clientCertificate: {
      thumbprint: AUTHENTICATION.thumbprint,
      privateKey: AUTHENTICATION.privateKey,
    },
  },
  system: {
    loggerOptions: {
      loggerCallback(loglevel, message, containsPii) {
        console.log(message);
      },
      piiLoggingEnabled: false,
      logLevel: 'Verbose',
    }
  }
};

const cca = new msal.ConfidentialClientApplication(config);

async function deletePastSessions(user) {
  console.log('Deleting existing session')

  const col = await client.getSession();

  col.deleteMany(
    { user: user },
  )
}

async function addUserToSession(id, user) {
  console.log('Adding user to session')

  const col = await client.getSession();

  col.updateOne(
    { _id: id },
    { $set: { user: user } }
  )
}

module.exports = {
  cca,
  deletePastSessions,
  addUserToSession,
}