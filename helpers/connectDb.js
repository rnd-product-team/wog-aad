const { MongoClient } = require("mongodb");
const { MONGODB } = require('../constants');

const client = new MongoClient(MONGODB.uri, MONGODB.options);

async function connect() {
  setInterval(async () => {
    console.log('Pinging MongoDB')
    try {
      await client.connect();
      const db = client.db(MONGODB.db);
      await db.command({ ping: 1 });
    } catch (err) {
      console.log(err);
    }
  }, 300000);
}

function getClient() {
  return client;
}

async function getSession() {
  await client.connect();
  const db = client.db(MONGODB.db);
  return db.collection(MONGODB.session);
}

module.exports = {
  connect,
  getClient,
  getSession,
};