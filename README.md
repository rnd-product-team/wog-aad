## Sign in flow
![sign in flow](sign_in_flow.svg)

## Sign out flow
![sign in flow](sign_out_flow.svg)

## Onboarding - create cert for domain
1. Create Certificate Signing Request (CSR); replace <cert_name> accordingly
   ```
   openssl req -new -newkey rsa:2048 -nodes -keyout <cert_name>.key -out <cert_name>.csr
   ```

   a. When prompted for "Common Name", fill in domain without https, www, or / at the end

   b. A private key will also be generated, which has to be added as an environment variable

2. Get cert signed by Certificate Authority (e.g. Comodo)

3. Generate fingerprint of signed cert
   ```
   openssl x509 -in <signed_cert_name>.crt -noout -fingerprint -sha1
   ```

   a. Remove ":" from generated fingerprint

   b. Add fingerprint as an environment variable

## Onboarding - ITSM
1. Become agency IT rep

2. Submit itsm.sgnet.gov.sg request

   a. Others > Azure Identity Services > AIS > Custom Enterprise Application (OpenID Connect) - Onboard Application

   b. Download template and fill it in. Fill "Reply URL" with "\<domain>/auth/reply"; replace \<domain> accordingly. Fill "MFA Enabled" with "Yes, for all users". Fill in the rest of the fields according to your application's requirements

   c. Attach the signed cert in the template

3. When ITSM request is completed, the tenant ID and client ID will be sent to you. Add these as environment variables