const dotenv = require('dotenv');
dotenv.config();
const { ServerApiVersion } = require("mongodb");
const fs = require('fs');

const ENDPOINTS = {
  managementPortal: process.env.ENDPOINT_MANAGEMENTPORTAL
};

const AUTHENTICATION = {
  clientId: process.env.AUTHENTICATION_CLIENT_ID,
  tenantId: process.env.AUTHENTICATION_TENANT_ID,
  thumbprint: process.env.AUTHENTICATION_THUMBPRINT,
  privateKey: fs.readFileSync(process.env.AUTHENTICATION_PRIVATEKEY, 'utf8'),
};

const MONGODB = {
  uri: process.env.MONGODB_URI,
  options: {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    sslKey: process.env.MONGODB_CRED,
    sslCert: process.env.MONGODB_CRED,
    serverApi: ServerApiVersion.v1,
  },
  db: process.env.MONGODB_DB,
  session: process.env.MONGODB_SESSION,
};

const SESSION = {
  secret: fs.readFileSync(process.env.SECRET, 'utf8'),
  expiry: 1000 * 60 * 30, // 30 mins
};

module.exports = {
  ENDPOINTS,
  AUTHENTICATION,
  MONGODB,
  SESSION,
};