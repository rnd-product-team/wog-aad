const {
  cca,
  deletePastSessions,
  addUserToSession,
} = require('../../helpers/auth');

const {
  ENDPOINTS,
} = require('../../constants');

const routeHandler = async (req, res, next) => {
  try {
    const tokenRequest = {
      code: req.body.code,
      scopes: ['openid'],
      redirectUri: `${ENDPOINTS.managementPortal}/auth/reply`,
    };

    cca.acquireTokenByCode(tokenRequest).then(async (response) => {
      const user = response.account.username.toLowerCase();
      console.log(`Signing in ${user}`);

      // single session per user
      await deletePastSessions(user);

      // regenerate the session, which is good practice to help
      // guard against forms of session fixation
      req.session.regenerate(function (err) {
        if (err) next(err);

        // store user information in session, typically a user id
        req.session.user = user;

        // load does not happen before session is saved
        req.session.save(async function (err2) {
          if (err2) return next(err2);
          await addUserToSession(req.session.id, user); // for helping to ensure single session per user

          return res.redirect(`${ENDPOINTS.managementPortal}/dashboard`);
        });
      })
    }).catch((error) => {
      console.log(error);
    });
  } catch (error) {
    console.log(error);
  }
};

module.exports = routeHandler;