const {
  AUTHENTICATION,
  ENDPOINTS,
} = require('../../constants');

const routeHandler = async (req, res, next) => {
  try {
    console.log(`Logging out for ${req.session.user}`);
    req.session.user = null;
    req.session.save((err) => {
      if (err) next(err);

      // regenerate the session, which is good practice to help
      // guard against forms of session fixation
      req.session.regenerate((err2) => {
        if (err2) next(err2);
        if (process.env.ENVIRONMENT == 'dev') {
          res.redirect(ENDPOINTS.managementPortal);
        } else {
          const logoutUri = `https://login.microsoftonline.com/${AUTHENTICATION.tenantId}/oauth2/v2.0/logout?post_logout_redirect_uri=${ENDPOINTS.managementPortal}`;
          res.redirect(logoutUri);
        }
      });
    });
  } catch (error) {
    console.log(error);
    return res.status(400).json({ error: error.message });
  }
};

module.exports = routeHandler;