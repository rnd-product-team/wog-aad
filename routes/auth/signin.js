const {
  cca,
  deletePastSessions,
  addUserToSession,
} = require('../../helpers/auth');

const {
  ENDPOINTS,
} = require('../../constants');

const routeHandler = async (req, res, next) => {
  try {
    if (process.env.ENVIRONMENT == 'dev') {
      console.log(`Logging in ${process.env.DEV_USER}`)
      await deletePastSessions(process.env.DEV_USER);
    }

    // regenerate the session, which is good practice to help
    // guard against forms of session fixation
    req.session.regenerate(function (err) {
      if (err) next(err);

      if (process.env.ENVIRONMENT == 'dev') {
        // store user information in session, typically a user id
        req.session.user = process.env.DEV_USER;

        // load does not happen before session is saved
        req.session.save(async function (err2) {
          if (err2) return next(err2);
          await addUserToSession(req.session.id, process.env.DEV_USER); // for helping to ensure single session per user

          return res.redirect(`${ENDPOINTS.managementPortal}/dashboard`);
        });
      } else {
        const authCodeUrlParameters = {
          scopes: ['openid'],
          redirectUri: `${ENDPOINTS.managementPortal}/auth/reply`,
          responseMode: 'form_post',
        };

        cca.getAuthCodeUrl(authCodeUrlParameters)
          .then((response) => {
            res.redirect(response);
          })
          .catch((error) => {
            console.log(error)
          });
      }
    })
  } catch (error) {
    console.log(error);
    return res.status(400).json({
      error: error.message,
    });
  }
};

module.exports = routeHandler;