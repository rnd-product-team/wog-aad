const express = require('express');
const router = express.Router();

router.get('/signin', require('./signin'));
router.post('/reply', require('./reply'));
router.get('/signout', require('./signout'));

router.get('/health', (req, res) => {
  res.sendStatus(200);
});

module.exports = router;
