const client = require('./helpers/connectDb');
client.connect();

const express = require('express');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const cors = require('cors');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const authRouter = require('./routes/auth');

const {
  ENDPOINTS,
  MONGODB,
  SESSION,
} = require('./constants');

let app = express();
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser());

// security settings
app.use(helmet({
  crossOriginResourcePolicy: { policy: "cross-origin" },
}));
app.use(helmet.contentSecurityPolicy({
  directives: {
    "style-src": ["'self'"],
    "base-uri": ["'none'"],
    "child-src": ["'self'"],
    "frame-ancestors": ["'none'"],
  },
}));
app.use(helmet.hsts({
  maxAge: 31536000,
}));
app.use(helmet.frameguard({
  action: "deny",
}));
app.use((req, res, next) => {
  res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate')
  next();
})
app.use((req, res, next) => {
  const allowedMethods = ['OPTIONS', 'GET', 'POST'];
  if (!allowedMethods.includes(req.method)) {
    res.status(405).send('Method not allowed');
  } else {
    next();
  }
});
app.use(cors({
  origin: ENDPOINTS.managementPortal.split(','),
  methods: ['GET', 'POST'],
  credentials: true
}));

// Middleware to set the 'x-forwarded-proto' header
app.use((req, res, next) => {
  req.headers['x-forwarded-proto'] = 'https';
  next();
});
// set up session
app.use(session({
  secret: SESSION.secret,
  proxy: true,
  cookie: {
    maxAge: SESSION.expiry,
    sameSite: true,
    secure: true,
  },
  store: MongoStore.create({
    client: client.getClient(),
    dbName: MONGODB.db,
    collectionName: MONGODB.session,
  }),
  rolling: true,
  resave: false,
  saveUninitialized: false,
}));

// set up routes
app.use('/auth', authRouter);

// start listening
const port = 3001;
app.listen(port, () => {
  console.log(`auth-app listening on port ${port}`);
});

// error handler
app.use((err, req, res, next) => {
  console.error(err);
  res.status(500).send('Internal server error');
});

module.exports = app;
